var ulElement = document.querySelector("#app ul");
var inputElement = document.querySelector("#app input");
var buttonElement = document.querySelector("#app button");

var lista = JSON.parse(localStorage.getItem("array_lista")) || [];

function renderLista() {
    
    ulElement.innerHTML = "";

    for (item of lista) {
        var aElement = document.createElement("a");
        var aText = document.createTextNode("Excluir");

        var pos = lista.indexOf(item);
        aElement.setAttribute("onclick", "deleteItem("+pos+")");

        aElement.setAttribute("href", "#");
        aElement.appendChild(aText);

        var liElement = document.createElement("li");
        var liText = document.createTextNode(item);

        liElement.appendChild(liText);
        liElement.appendChild(aElement);

        ulElement.appendChild(liElement);
    }
}

renderLista();

function adicionarLista() {
    var valorInput = inputElement.value;
    lista.push(valorInput);
    inputElement.value = '';

    renderLista();
    salvarToStorage();
}

buttonElement.onclick = adicionarLista;

function deleteItem(pos) {
    lista.splice(pos, 1);
    renderLista();
    salvarToStorage();
}

function salvarToStorage() {
    //global 
    localStorage.setItem("array_lista", JSON.stringify(lista));
}