function checaIdade(idade){
    return new Promise(function(resolve, reject){

        setTimeout(function() {
            
            if(idade > 18 && idade > 0){
                resolve();
            }
    
            if(idade < 18 && idade > 0){
                reject();
            }
        }, 2000);
        

    });
}

var valor;

var divElement = document.querySelector("div");
var inputElement = document.createElement("input");
inputElement.setAttribute("type", "number");

divElement.appendChild(inputElement);

var buttonElement = document.createElement("button");
var buttonText = document.createTextNode("Enviar value");
buttonElement.appendChild(buttonText);

divElement.appendChild(buttonElement);

buttonElement.onclick = function() {
    valor = parseFloat(inputElement.value);
    checaIdade(valor)
    .then(function() {
        console.log("Idade maior que 18");
    })
    .catch(function() {
        console.log("Idade menor que 18");
    });
}

