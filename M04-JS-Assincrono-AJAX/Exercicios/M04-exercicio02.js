var inputElement = document.querySelector("input");
var buttonElement = document.querySelector("button");
var ulElement = document.querySelector("ul");


function renderLista(repositorio) {
    for (repos of repositorio) {
        var liElement = document.createElement("li");
        var textoElement = document.createTextNode(repos.name);

        liElement.appendChild(textoElement);
        ulElement.appendChild(liElement);
    }
} 


buttonElement.onclick = function() {
    
    var user = inputElement.value;

    if(!user) return;

    axios.get("https://api.github.com/users/"+ user +"/repos")
        .then(function(resolve) {
            renderLista(resolve.data);
        })

    /*
    var promessa = function() {


        return new Promise(function(resolve, reject) {

            var xml = new XMLHttpRequest();
            
            var user = inputElement.value

            if(!user) return;

            xml.open("GET", "https://api.github.com/users/"+ user +"/repos");
            xml.send(null);

            xml.onreadystatechange = () => {
                
                if(xml.readyState !== 4) return;
                
                (xml.status === 200) ? resolve(JSON.parse(xml.responseText)) : reject("Erro na requisição");

            }

        });

    } 

    promessa()
    .then(function(resolve) {
        console.log(resolve);
        renderLista(resolve);
    })
    .catch(function(reject) {
        console.warn(reject);
    })
    **/
}