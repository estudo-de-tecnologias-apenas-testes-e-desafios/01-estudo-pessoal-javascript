var ajax = new XMLHttpRequest();

ajax.open("GET", "https://api.github.com"); 
ajax.send(null);


// Se eu tentar dá um ajax não vai retornar nada, pois a requisição aconteceu depois do codigo
// executado. Por Isso onReadyStateChange
ajax.onreadystatechange = function() {
    if(ajax.readyState === 4 ) { // 4 Significa que a resposta retornou

        console.log(JSON.parse(ajax.responseText)); //retorna o conteudo da API

    }
}