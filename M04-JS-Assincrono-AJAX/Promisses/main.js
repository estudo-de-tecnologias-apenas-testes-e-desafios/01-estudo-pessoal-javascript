var promise = function () {

    return new Promise(function (resolve, reject) {

        var xmlAjax = new XMLHttpRequest();
        xmlAjax.open("GET", "http://api.github.com");
        xmlAjax.send(null);

        xmlAjax.onreadystatechange = function () {

            if (xmlAjax.readyState === 4) {

                if (xmlAjax.status === 200) {
                    resolve(JSON.parse(xmlAjax.responseText));
                } else {
                    reject("Erro na Requisição");
                }
            }

        }

    });

}

promise()
.then(function(response){
    console.log(response);
})
.catch(function(error){
    console.warn(error);
})